package softwareproj;

public class DoctorController {
	
	Doctor dc = new Doctor();
	
	public String getName(String ID)
	{
		String result = dc.getName(ID);
		return result.toUpperCase();
	}
	
	public String getPassword(String ID)
	{
		String result = dc.getPassword(ID);
		return result;
	}
	

}
