package softwareproj;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import softwareproj.UserEditController;
import softwareproj.DoctorController;

import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;

public class DoctorGui extends JFrame {

	private JFrame frame;
	private JTextField txtID;
	private JTextField txtName;
	private JPasswordField CurrentPW;
	private JPasswordField NewPW;
	private JTextField txtPrescription;
	private JTextField textIDv;
	private JTextField txtTraceID;
	private JTable VisitedTable;

	public DoctorGui(String ID) {
		display(ID);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void display(String ID) {
		frame = new JFrame("Doctor's GUI");
		frame.setBounds(100, 100, 478, 397);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		frame.setVisible(true);

		JPanel DoctorGui = new JPanel();
		frame.getContentPane().add(DoctorGui, "name_411284600144100");
		DoctorGui.setLayout(null);
		DoctorGui.setVisible(true);

		JPanel PastGui = new JPanel();
		frame.getContentPane().add(PastGui, "name_411284610309900");
		PastGui.setLayout(null);

		JPanel ProfileGui = new JPanel();
		frame.getContentPane().add(ProfileGui, "name_411284610309900");
		ProfileGui.setLayout(null);

		JLabel lblID_1 = new JLabel("ID:");
		lblID_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblID_1.setBounds(50, 26, 25, 23);
		ProfileGui.add(lblID_1);

		JLabel lblWelcome = new JLabel("");
		lblWelcome.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblWelcome.setBounds(10, 11, 284, 26);
		DoctorGui.add(lblWelcome);
		lblWelcome.setText("Welcome, " + ID.toUpperCase());

		JButton btnLogout = new JButton("Log out");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LoginGui();
				frame.dispose();
			}
		});
		btnLogout.setBounds(304, 11, 145, 23);
		DoctorGui.add(btnLogout);

		txtID = new JTextField();
		txtID.setText((String) null);
		txtID.setEditable(false);
		txtID.setColumns(10);
		txtID.setBounds(85, 29, 137, 20);
		ProfileGui.add(txtID);

		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName_1.setBounds(30, 55, 45, 23);
		ProfileGui.add(lblName_1);

		txtName = new JTextField();
		txtName.setText((String) null);
		txtName.setEditable(false);
		txtName.setColumns(10);
		txtName.setBounds(85, 58, 137, 20);
		ProfileGui.add(txtName);

		JLabel lblchangePW = new JLabel("Change Password");
		lblchangePW.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblchangePW.setBounds(30, 206, 155, 29);
		ProfileGui.add(lblchangePW);

		JLabel lblPW = new JLabel("Current PW:");
		lblPW.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPW.setBounds(30, 233, 83, 23);
		ProfileGui.add(lblPW);

		JLabel lblNewPW = new JLabel("New PW:");
		lblNewPW.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewPW.setBounds(30, 267, 83, 23);
		ProfileGui.add(lblNewPW);

		CurrentPW = new JPasswordField();
		CurrentPW.setBounds(127, 236, 134, 20);
		ProfileGui.add(CurrentPW);

		NewPW = new JPasswordField();
		NewPW.setBounds(127, 270, 134, 20);
		ProfileGui.add(NewPW);

		JButton ChangePassword = new JButton("Change Password");
		ChangePassword.setBounds(107, 293, 175, 23);
		ProfileGui.add(ChangePassword);

		JLabel lblresult2 = new JLabel("");
		lblresult2.setBounds(30, 327, 428, 20);
		ProfileGui.add(lblresult2);

		JButton Home = new JButton("Home");
		Home.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorGui.setVisible(true);
				ProfileGui.setVisible(false);
			}
		});
		Home.setBounds(307, 52, 145, 23);
		ProfileGui.add(Home);

		JButton Logout = new JButton("Log out");
		Logout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LoginGui();
				frame.dispose();
			}
		});
		Logout.setBounds(307, 11, 145, 23);
		ProfileGui.add(Logout);

		JButton btnViewProfile = new JButton("View Profile");
		btnViewProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorController dc = new DoctorController();
				txtID.setText(ID.toUpperCase());
				txtName.setText(dc.getName(ID).toUpperCase());
				ProfileGui.setVisible(true);
				DoctorGui.setVisible(false);
				CurrentPW.setText("");
				NewPW.setText("");
				lblresult2.setText("");
			}
		});
		btnViewProfile.setBounds(307, 39, 145, 23);
		DoctorGui.add(btnViewProfile);

		ChangePassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentPW = String.valueOf(CurrentPW.getPassword());
				String newPW = String.valueOf(NewPW.getPassword());
				DoctorController hsc = new DoctorController();

				UserEditController ec = new UserEditController();
				if (ec.validateData(currentPW, newPW, hsc.getPassword(ID))) {
					if (ec.changePassword(newPW, ID)) {
						lblresult2.setText("Password updated.");
					} else {
						lblresult2.setText("Failed to update password.");
					}
				} else {
					lblresult2.setText("Please make sure you entered the correct values");
				}
			}
		});

		JPanel IssuePrescription = new JPanel();
		frame.getContentPane().add(IssuePrescription, "name_197157197342200");
		IssuePrescription.setLayout(null);

		JButton Issue = new JButton("Issue");
		Issue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorGui.setVisible(false);
				IssuePrescription.setVisible(true);
			}
		});
		Issue.setBounds(105, 141, 229, 23);
		DoctorGui.add(Issue);

		JButton btnNewButton = new JButton("View Patient Past Records");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PastGui.setVisible(true);
				DoctorGui.setVisible(false);
			}
		});
		btnNewButton.setBounds(105, 175, 229, 26);
		DoctorGui.add(btnNewButton);

		JLabel lbllocation = new JLabel("ID: ");
		lbllocation.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbllocation.setBounds(110, 74, 75, 24);
		IssuePrescription.add(lbllocation);

		JComboBox<Object> comboPrescription = new JComboBox<Object>();
		comboPrescription.setModel(new DefaultComboBoxModel<Object>(
				new String[] { "Flu Medicine", "Cough Medcine", "Fever Medicine", "J Medicine" }));
		comboPrescription.setBounds(140, 138, 207, 22);
		IssuePrescription.add(comboPrescription);

		textIDv = new JTextField();
		textIDv.setBounds(140, 78, 207, 20);
		IssuePrescription.add(textIDv);
		textIDv.setColumns(10);

		JButton btnIssuePrescription = new JButton("Issue");
		btnIssuePrescription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				DoctorIssuePrescriptionController pumc = new DoctorIssuePrescriptionController();
				String ID = textIDv.getText();
				String Prescription = comboPrescription.getSelectedItem().toString();

				if (!pumc.checkID(ID)) {
					JOptionPane.showMessageDialog(frame, "ID should not be empty!");
				}

				else if (pumc.validateID(ID)) {
					JOptionPane.showMessageDialog(frame, "ID doesnt exist!");
				} else {
					pumc.IssuePrescription(ID, Prescription);
					pumc.IssuePrescription(ID);
					JOptionPane.showMessageDialog(frame, "Prescription Status Updated");
					textIDv.setText("");
				}
			}
		});
		btnIssuePrescription.setBounds(160, 272, 145, 39);
		IssuePrescription.add(btnIssuePrescription);

		JButton btnHome_1 = new JButton("Home");
		btnHome_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorGui.setVisible(true);
				IssuePrescription.setVisible(false);
			}
		});
		btnHome_1.setBounds(307, 39, 145, 23);
		IssuePrescription.add(btnHome_1);

		txtTraceID = new JTextField();
		txtTraceID.setBounds(78, 17, 124, 20);
		PastGui.add(txtTraceID);
		txtTraceID.setColumns(10);

		JButton btnTrace = new JButton("Check Prescription");
		btnTrace.setBounds(215, 16, 114, 23);
		PastGui.add(btnTrace);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 110, 438, 190);
		PastGui.add(scrollPane);

		VisitedTable = new JTable() {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		VisitedTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(VisitedTable);

		JLabel lblNewLabel = new JLabel("ID");
		lblNewLabel.setBounds(22, 20, 49, 14);
		PastGui.add(lblNewLabel);

		JButton btnNewButton_1 = new JButton("Home");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorGui.setVisible(true);
				PastGui.setVisible(false);
			}
		});
		btnNewButton_1.setBounds(348, 16, 89, 23);
		PastGui.add(btnNewButton_1);

		btnTrace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorViewPrescriptionController ctc = new DoctorViewPrescriptionController();
				String TraceID = txtTraceID.getText();
				if (ctc.checkID(TraceID)) {
					displayPrescription(TraceID);
				} else {
					JOptionPane.showConfirmDialog(null, "Please enter ID!", "Error", JOptionPane.DEFAULT_OPTION,
							JOptionPane.PLAIN_MESSAGE);
				}
			}
		});

	}

	public void displayPrescription(String Prescription) {
		DoctorViewPrescriptionController ctc = new DoctorViewPrescriptionController();
		Vector<Details> v = ctc.displayPrescription(Prescription);
		DefaultTableModel model = new DefaultTableModel(new String[] { "Prescription" }, 0);
		for (Details d : v) {
			model.addRow(new Object[] { d.v });
		}
		VisitedTable.setModel(model);
	}

}
